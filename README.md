# Internship Interview Questions

1. In Java, the maximum size of an Array needs to be set upon initialization. Supposedly, we want something like an Array that is dynamic, such that we can add more items to it over time. Suggest how we can accomplish that (other than using ArrayList)?  

**_I think we can create a function where whenever an item is to be added to that Array, a new array of bigger size can be created, maybe with the size of the old array + number of items to be added. Then, the items from the old Array can be copied to that new Array, add the new items to the new Array, and then the function returns this new Array._**  

2. Explain this block of code in Big-O notation.
    ```
    void sampleCode(int arr[], int size)
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                printf("%d = %d\n", arr[i], arr[j]);
            }
         }
    }
    ```

**_Assuming n = size, O(n^2). A for loop with conditions (int i=0;i < n; i++) iterates n times. The code iterates through two for loops, thus time complexity of two nested for loops is O(n^2)_**


3. You are a web developer working on a web application. Customer support approached you and escalated a problem that users are currently facing. The user visited the page at http://this-is-a-sample-site.com/shop/items and while the web page actually loads, the list of items for sale is stuck at loading. It seems to be related to Javascript. As a web developer, how do you diagnose this and the approach you would take to narrow down the problem?

**_Since the page is loading but the list of items for sale is not, I would try to test if there is a problem with fetching the items. I'd try checking the database and the blocks of code which fetch the item list. I'd confirm that I am fetching the right items and double checking the variable names, console logging to debug which parts of codes are being ran and causing the loading problem to occur._**      

4. You have learnt languages like C and C++ in university. It is said that languages like C and C++ is powerful, much more powerful than languages like Java, Python, Ruby, etc.. Is it fair to say that all softwares should be written in C and C++? Why and why not?

**_I think that when C and C++ were invented, it was for different purposes. For example, C and C++ were not created for web development or object-oriented programming. So, high-level languages like Java and Python were created to specialise in other areas and provide a more stable and smooth development experience. C and C++ also do not have as much plug-ins and library support as other modern languages, making it not beginner-friendly. Besides, I think these languages do not require as much system control as C and C++ has and provides a better experience for developers to develop a software. Thus, I do not think it is fair to say that all softwares should be written in C and C++._** 

---
# Simple Coding Assessment

[Crypto Dashboard](https://cgcrypto-dashboard.vercel.app/)
